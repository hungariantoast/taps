# Tildes Automated Posting Script

TAPS is a script designed to automatically post a topic to
[Tildes](https://tildes.net), a non-profit community site.

When used in conjunction with something like
[cron](https://en.wikipedia.org/wiki/Cron), TAPS can be used to post topics
on a schedule.

Topics are configured in the `config.py` file. Configuration instructions
can be found below.

To run the script, execute the `taps.py` file
with the name of one of the topics from `config.py` as an argument.

So, to post the topic named "example" in `config.py` you would,
after configuring your username and password, run:

`taps.py example`

To post *all* topics configured in `config.py` you would pass `all` as the
argument to the script:

`taps.py all`

## Requirements

* [Python 3](https://www.python.org/)
* [tildee](https://git.dingenskirchen.systems/Dingens/tildee.py)
  * [tildee Pip Package](https://pypi.org/project/tildee/)

## Configuration

### Setup

1. Download and install Python 3 and Pip
2. Install tildee using Pip
3. Download or clone this repository

### `config.py`

Once you have downloaded this repository you will need to change a few things in
`config.py` before the script will work:

* `username`: You should set this variable to the account username that you want
topics to be posted under.
* `password`: Set this to the password for the account you want to use.
* `totp`: Set this to `True` if you use two-factor authentication.
* `base_url`: The URL the script tries to post content to. Unless you're working
in a development environment you shouldn't need to change this.
* `ratelimit`: This controls how long the program waits between actions. Setting
the value any lower *might* trigger the site's rate limit and break the script.
* `repost`: If this is set to `False`, then the program won't post a link topic
if a topic with that link has already been posted. Setting this to `True` will
automatically repost links.

---

### Configuring Topics

The final variable in `config.py` is `topics`. This variable is a
list of dictionaries, with each dictionary defining a topic for the script to
post.

---

The first key is `"name"`. The value for this key is the name (**not** the
title) that you want to set for this topic in the script. The name is what you
will pass as an argument when you run the script to actually post the topic.

For example, `config.py` has an example topic written out
named "example". If I wanted to post this topic to Tildes, I would have to run
TAPS by executing the script with the topic name "example" as an argument:

`taps.py example`

Just running `taps.py` without
passing a topic name as an argument **will not** post anything.

You can post multiple topics at once by passing multiple names as arguments:

`taps.py example example2 example-three`

---

The second key is `"group"`. The value for this key is the group you would like
to post the topic to on Tildes. In the example topic, the ~test group is the
value.

---

The third key is `"title"`. The value for this key is the title that you would
like the topic to have when it is posted to Tildes. If your title contains
quotation "marks", you can set three marks (""")
on each side of the topic to ignore marks in the title:
`"title": """your-"topic"-title-here""",`

---

The fourth key is `"link"`. The value for this key is the link that you would
like the topic to point to. This is optional on Tildes, but if you *don't*
include a link, you ***must*** include a comment, defined by the next key.

---

The fifth key is `"comment"`. This value is the comment that you would like to
include in your topic. If your topic includes a link, then a comment is not
necessary. However, if your topic *doesn't* include a link, then you
***must*** include a comment with your topic.

---

The sixth key is `"tags"`. This value is a list of all the tags that you would
like your topic to be tagged with. This is optional, but highly encouraged.

---

The seventh and eighth keys are `"username"` and `"password"`. These keys can
optionally be left blank, and the script will post the topic under the username
and password defined in the `username` and `password` variables at the top of
`config.py`. However, if you *do* set username and password values for these
keys, the script will instead use the account those credentials belong to
when posting the topic.

So, if I have defined my username and password for my hungariantoast account
on Tildes at the top of `config.py`, but in a topic I give the `"username"` and
`"password"` keys the values for my tautobot account on Tildes, that topic will
instead be posted by tautobot, **not** hungariantoast.

---

The final key is `"totp"`. This only needs to be set to a value if the
`"username"` and `"password"` keys for that topic have been set to an account
that uses two-factor authentication. This **is not** the TOTP
code you'll need to enter to post a topic. The script will ask you for that code
when executed. This value accepts anything, even gibberish.
