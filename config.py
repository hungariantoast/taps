# Usage: taps.py example
#        taps.py all
username = "your-username-here"
password = "your-password-here"
totp = False
# totp = True
base_url = "https://tildes.net"
# base_url = "https://localhost:4443"
ratelimit = 1000
repost = False
# repost = True

topics = [
  {
    "name": "example",
    "group": "test",
    "title": "your-topic-title-here",
    "link": "https://tildes.example.net",
    "comment":
"""
I know this looks weird, not being indented.

But:

It makes writing comments (and formatting them correctly) easier
""",
    "tags": ["taps", "tautobot", "neato", "text"],
    "username": "",
    "password": "",
    "totp": ""
    # "totp": "asdfghjkl"
  },
  {
    "name": "",
    "group": "",
    "title": "",
    "link": "",
    ### IMPORTANT: If your comment field is empty and you *don't* format the marks
    # Like this, the script will fail. It's a known issue.
    "comment":
    """""",
    "tags": ["taps", "tautobot", "neato", "text"],
    "username": "",
    "password": "",
    "totp": ""
  },
]
