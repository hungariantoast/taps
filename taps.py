#!/usr/bin/env python3
import sys
from tildee import TildesClient
import config


def post_topic(topic):
    if topic['username'] != '':
        topic_username = topic['username']
    else:
        topic_username = config.username
    if topic['password'] != '':
        topic_password = topic['password']
    else:
        topic_password = config.password
    if config.base_url != 'https://tildes.net':
        verify_ssl = False
    else:
        verify_ssl = True
    if topic['totp'] != '':
        totp = True
    else:
        totp = config.totp
    if totp == False:
        t = TildesClient(username=topic_username, password=topic_password,
                         base_url=config.base_url, verify_ssl=verify_ssl)
    elif totp == True:
        topic_code = input("Please enter TOTP code: ")
        t = TildesClient(username=topic_username, password=topic_password,
                         base_url=config.base_url, verify_ssl=verify_ssl, totp_code=topic_code)
    if topic['link'] == '':
        t.create_topic(topic['group'], topic['title'],
                       topic['tags'], markdown=topic['comment'])
    if topic['link'] != '':
        topic_id = t.create_topic(
            topic['group'], topic['title'], topic['tags'], link=topic['link'],  confirm_repost=config.repost)
        if topic['comment'] != '':
            t.create_comment(topic_id, topic['comment'])


for argument in sys.argv[1:]:
    if argument == 'all':
        for topic in config.topics:
            post_topic(topic)
    else:
        for topic in config.topics:
            if 'name' in topic:
                if argument == topic['name']:
                    post_topic(topic)
